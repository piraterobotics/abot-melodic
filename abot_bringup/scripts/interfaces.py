#!/usr/bin/env python

import sys
import netifaces

print("Checking interface cards")

for inf in netifaces.interfaces():
	print("index ", inf)
	address = netifaces.ifaddresses(inf)
	print("address: ", address)
	for i, ifx in enumerate(address):
		print("ia, ifx: ", i, ifx)
	next
next
